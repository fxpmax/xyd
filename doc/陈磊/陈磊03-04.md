# 个人周报
## 上两周个人计划及本周完成情况

- [x] 了解github的开发模式与流程
- [x] 进行scrum站立会议
- [x] 对界面设计进行修改
- [x] 制定项目的sprint backlog
- [x] 完善项目的product backlog

## 遇到的问题
1.对于product backlog，有些用户故事还是太大，不够细致详尽。  
解决方案：虚心听取老师的意见，之后多结合具体的情况，对项目的product backlog进行改进。   
  
## 下周学习及开发计划
1.尽力完成第一次迭代的开发  
2.对代码和app进行检查和测试  
3.进行review meeting审评会