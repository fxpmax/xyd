package com.example.ssdapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by 范晋瑜 on 2017/10/10.
 */

public class AlterActivity extends Activity {
    private Button mButton_commit,mButton_cancel;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.fragment_alter);

        mButton_commit = (Button)findViewById(R.id.commit);
        mButton_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(),"修改成功",Toast.LENGTH_SHORT).show();
                setResult(2);
                finish();
            }
        });
        mButton_cancel = (Button)findViewById(R.id.cancel);
        mButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(3);
                finish();
            }
        });

   }
}
