package com.example.ssdapp;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by 范晋瑜 on 2017/10/9.
 */

public class QdfFragment extends Fragment {
    private Button mButton_dwm;
    private Button mButton_dfc;
    private Button mButton_cancel;
    private View v;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        v = inflater.inflate(R.layout.fragment_qdf,container,false);
        InitView();
        mButton_dwm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                alert_dwm();
            }
        });
        mButton_dfc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                alert_dfc();
            }
        });
        mButton_cancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V) {
                alert_cancel();
            }
        });
        return v;
    }

    private void alert_cancel() {

    }

    private void alert_dfc() {
        LayoutInflater inflater=LayoutInflater.from(getContext());
        View view= inflater.inflate(R.layout.self_qfc,null);
        final EditText text = (EditText)view.findViewById(R.id.dfc_introduce);
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        builder.setTitle("要求");
        builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface arg0,int arg1){
//                        发送带饭菜信息
            }
        });
        builder.setNegativeButton("取消",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface arg0,int arg1){
            }
        });
        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void alert_dwm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("提示");
        builder.setMessage("确定带外卖？");
        builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface arg0,int arg1){
                                //发送带外卖信息
            }
        });
        builder.setNegativeButton("取消",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface arg0,int arg1){
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void InitView() {
        mButton_dwm = (Button)v.findViewById(R.id.button_dwm);
        mButton_dfc = (Button)v.findViewById(R.id.button_dfc);
        mButton_cancel=(Button)v.findViewById(R.id.button_cancel);
    }

}
