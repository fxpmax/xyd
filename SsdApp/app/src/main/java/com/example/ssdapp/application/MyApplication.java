package com.example.ssdapp.application;

import android.app.Application;

import com.itheima.retrofitutils.ItheimaHttp;

/**
 * Created by 范晋瑜 on 2017/10/23.
 */

public class MyApplication extends Application {
    private String url = "http://123.206.107.125:3333/";

    @Override
    public void onCreate() {
        super.onCreate();
        ItheimaHttp.init(this, url);
    }
}
