package com.example.ssdapp.Activity;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ssdapp.MyFragment;
import com.example.ssdapp.QdfFragment;
import com.example.ssdapp.R;
import com.example.ssdapp.SsdFragment;

public class MainActivity extends FragmentActivity {

    private TextView mButtonQdf;
    private TextView mButtonSsd;
    private TextView mButtonMy;
    private TextView title;
    private Fragment currentFragment;

    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment = fm.findFragmentById(R.id.fragment_main);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("fjy-run-current=",LogActivity.CurrentUser);
        title = (TextView)findViewById(R.id.title);
        title.setText("求带飞");

        fragment = new QdfFragment();
            fm.beginTransaction().add(R.id.fragment_main,fragment).commit();
        currentFragment = fragment;

        mButtonQdf = (TextView) findViewById(R.id.button_qdf);
        mButtonQdf.setBackgroundColor(Color.parseColor("#FFFFFF"));
        mButtonQdf.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mButtonQdf.setBackgroundColor(Color.parseColor("#FFFFFF"));
                mButtonSsd.setBackgroundColor(Color.parseColor("#FF0000"));
                mButtonMy.setBackgroundColor(Color.parseColor("#FF0000"));
                showFragment(new QdfFragment());
                title.setText("求带飞");
            }
        });

        mButtonSsd = (TextView) findViewById(R.id.button_ssd);
        mButtonSsd.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mButtonSsd.setBackgroundColor(Color.parseColor("#FFFFFF"));
                mButtonQdf.setBackgroundColor(Color.parseColor("#FF0000"));
                mButtonMy.setBackgroundColor(Color.parseColor("#FF0000"));
                showFragment(new SsdFragment());
                title.setText("顺手带");
            }
        });

        mButtonMy = (TextView) findViewById(R.id.button_my);
        mButtonMy.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mButtonMy.setBackgroundColor(Color.parseColor("#FFFFFF"));
                mButtonSsd.setBackgroundColor(Color.parseColor("#FF0000"));
                mButtonQdf.setBackgroundColor(Color.parseColor("#FF0000"));
                showFragment(new MyFragment());
                title.setText("我的");
            }
        });
    }
    public void showFragment(Fragment fg){
        if(!fg.isAdded())
        {
            fm.beginTransaction()
                    .hide(currentFragment)
                    .add(R.id.fragment_main,fg)
                    .commit();
        }else{
            fm.beginTransaction()
                    .hide(currentFragment)
                    .show(fg)
                    .commit();
        }
        currentFragment = fg;
    }

}
