package com.example.ssdapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ssdapp.R;
import com.example.ssdapp.bean.Post;
import com.example.ssdapp.bean.User;
import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;

/**
 * Created by 范晋瑜 on 2017/10/11.
 */

public class LogActivity extends AppCompatActivity{
    private Button log,register;
    private EditText number;
    private EditText password;
    private User mUser;
    private static int n=2;
    public static String CurrentUser="pp";
    private static final String loginUrl = "login";
    private String userNumber;
    private String userPassword;
    String loginJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        initView();
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNameAndPassword();
                T1 t1 = new T1();
                t1.setPriority(2);
                t1.start();

                T2 t2 = new T2();
                t2.setPriority(1);
                t2.start();
            }
        });
        register= (Button)findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogActivity.this,RegisterActivity.class);
                startActivity(i);
            }
        });
    }

    private void getNameAndPassword() {
        userNumber = number.getText().toString();
        userPassword = password.getText().toString();
    }
    class T1 extends Thread{
        @Override
        public void run() {
            login();
        }
    }
    class T2 extends Thread{
        @Override
        public void run() {
            Log.i("fjy-run-current=",CurrentUser);
        }
    }
    //用不了，原来用的是okhttp，这里用的是retrofit
    private void login() {
        if(userNumber.equals(""))
            clear("账号不能为空");
        else if(!userNumber.equals("")&&userPassword.equals(""))
            clear("密码不能为空");
        else {
            Request request = ItheimaHttp.newPostRequest(loginUrl);

            request.putParams("username", userNumber);
            request.putParams("userpassword", userPassword);

            Call call = ItheimaHttp.send(request, new HttpResponseListener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i("fjy", s);
                    try {
                        JSONObject json = new JSONObject(s);
                        if (json.getString("ok").equals("1")) {
                            CurrentUser = userNumber;
                            Log.i("fjy-current=",CurrentUser);
                            Toast.makeText(getBaseContext(),"登录成功",Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(LogActivity.this,MainActivity.class);
                            startActivity(i);
                        }
                        else Toast.makeText(getBaseContext(),"账号或密码不正确",Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    private void initView() {
        number = (EditText)findViewById(R.id.number_edit);
        password = (EditText)findViewById(R.id.password_edit);
        password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        log = (Button)findViewById(R.id.log);
    }

    public void clear(String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

}
