package com.example.ssdapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ssdapp.Activity.LogActivity;
import com.example.ssdapp.bean.Order;
import com.example.ssdapp.bean.OrderList;
import com.example.ssdapp.bean.Post;
import com.example.ssdapp.bean.User;
import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 范晋瑜 on 2017/10/9.
 */

public class SsdFragment extends Fragment {

    private RecyclerView mOrderRecyclerView;
    private OrderAdapter mAdapter;
    private AlertDialog.Builder builder;
    private static String order_url="";
    private static String user_url="";
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_ssd,container,false);

        mOrderRecyclerView = (RecyclerView)v.findViewById(R.id.list_ssd);
        mOrderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        return v;
    }

    private void updateUI() {
        OrderList orderList = OrderList.get(getActivity());
        List<Order> orders = orderList.getOrders();
        mAdapter = new OrderAdapter(orders);
        mOrderRecyclerView.setAdapter(mAdapter);
    }

    private class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView id,user,name,floor,room,tag;
        private Order mOrder;

        public OrderHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            id = (TextView)itemView.findViewById(R.id.order_id);
            user = (TextView)itemView.findViewById(R.id.order_user);
            name = (TextView)itemView.findViewById(R.id.order_name);
            floor = (TextView)itemView.findViewById(R.id.order_floor);
            room = (TextView)itemView.findViewById(R.id.order_room);
            tag = (TextView)itemView.findViewById(R.id.order_tag);
        }
        public void bindOrder(Order order){
            mOrder = order;
            id.setText(order.getId());
            //user.setText(order.getUser().getNumber());
            name.setText(order.getUser().getName());
            floor.setText(order.getUser().getFloor());
            room.setText(order.getUser().getRoom());
            tag.setText(order.getTag());
        }
        @Override
        public void onClick(View v) {

        }
    }
    private class OrderAdapter extends RecyclerView.Adapter<OrderHolder>{
        private List<Order> mOrders;
        private OrderAdapter(List<Order> orders){
            mOrders = orders;
        }
        @Override
        public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layout = LayoutInflater.from(getActivity());
            View view = layout.inflate(R.layout.ssd_layout,parent,false);
            return new OrderHolder(view);
        }

        @Override
        public void onBindViewHolder(OrderHolder holder, int position) {
            Order order = mOrders.get(position);
            holder.bindOrder(order);
        }

        @Override
        public int getItemCount() {
            return mOrders.size();
        }
    }
    private void tip(final View view, final int position){
        builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("提示");
            builder.setMessage("您要放弃此任务吗？");
            builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface arg0,int arg1){
//                    removeOrder(ssdList.get(position).get("who"));
//                    adapter.notifyDataSetChanged();
                    show("您已放弃此任务");
                }
            });
            builder.setMessage("确定帮带吗？");
            builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface arg0,int arg1){
//                            removeOrder(ssdList.get(position).get("who"));
//                            addOrderToNodone(ssdList.get(position).get("who"));
//                            adapter.notifyDataSetChanged();
                            show("'带外卖'任务已添加到‘未完成任务’");
                        }
                    }
            );
            builder.setNegativeButton("取消",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface arg0,int arg1){

                        }
                    }
            );
        //adapter.notifyDataSetChanged();
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addOrderToNodone(String who) {
        //添加订单到未完成
    }

    private void removeOrder(String who) {
        //删除一个发布订单
    }
    public void show(String s){
    Toast.makeText(getContext(),s,Toast.LENGTH_SHORT).show();
}
}
