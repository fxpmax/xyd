package com.example.ssdapp.bean;

import android.util.Log;

import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 范晋瑜 on 2017/10/23.
 */

public class Call {
    private String s1,s2;
    public  void Post(String url){
        Request request = ItheimaHttp.newPostRequest(url);
        request.putParams("username",s1);
        request.putParams("userpassword",s2);
        retrofit2.Call call = ItheimaHttp.send(request, new HttpResponseListener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("fjy", s);
                try {
                    JSONObject json = new JSONObject(s);
                    final String ok = json.getString("ok");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    public void Get(String url){
        Request request = ItheimaHttp.newGetRequest(url);
        retrofit2.Call call = ItheimaHttp.send(request, new HttpResponseListener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("fjy", s);
                try {
                    JSONObject json = new JSONObject(s);
                    final String ok = json.getString("ok");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
