package com.example.ssdapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by 范晋瑜 on 2017/10/10.
 */

public class MyDoneFragment extends Fragment {
    private ListView mListView;
    private SimpleAdapter mAdapter;
    private List<Map<String,String>> ssdList;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_done, container, false);

//        ssdList = new ArrayList<>();
//        ssdList = null;
//
//        mListView = (ListView)v.findViewById(R.id.list_done);
//        mAdapter=new SimpleAdapter(getContext(),ssdList,R.layout.ssd_layout,new String[]{"who","vwho","what","from","to"},
//                new int[]{R.id.who,R.id.vwho,R.id.what,R.id.from,R.id.to});
//        mListView.setAdapter(mAdapter);
//
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                builder.setTitle("提示");
//                builder.setMessage("删除此任务？");
//                builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
//                    @Override
//                    public void onClick(DialogInterface arg0,int arg1){
//
//                        Toast.makeText(getContext(),"删除成功",Toast.LENGTH_SHORT).show();
//                        //mAdapter.notifyDataSetChanged();
//                    }
//                });
//                builder.setNegativeButton("取消",new DialogInterface.OnClickListener(){
//                    @Override
//                    public void onClick(DialogInterface arg0,int arg1){
//
//                    }
//                });
//
//                AlertDialog dialog = builder.create();
//                dialog.show();
//            }
//        });
        return v;
    }
}
