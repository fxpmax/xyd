package com.example.ssdapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by 范晋瑜 on 2017/10/10.
 */

public class MyNodoneFragment extends Fragment {
    private ListView mListView;
    private SimpleAdapter mAdapter;
    private View v;
    private AlertDialog.Builder builder;
    private List<Map<String,String>> ssdList;
    private final String TAG = getClass().getSimpleName();
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.my_nodone, container, false);
//        InitView();
//
//        mListView = (ListView)v.findViewById(R.id.list_nodone);
//        mAdapter=new SimpleAdapter(getContext(),ssdList,R.layout.ssd_layout,new String[]{"id","who","vwho","what","from","to"},
//                new int[]{R.id.id,R.id.who,R.id.vwho,R.id.what,R.id.from,R.id.to});
//        mListView.setAdapter(mAdapter);
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                builder = new AlertDialog.Builder(getActivity());
//                builder.setTitle("提示");
//                Alert();
//            }
//        });
        return v;
        }

    private void InitView() {
        ssdList = new ArrayList<>();
        ssdList = null;
    }


    private void Alert() {
        builder.setMessage("确定此任务已完成？");
        builder.setPositiveButton("确定",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface arg0,int arg1){

                            //更新数据
                            MyFragment myFragment = (MyFragment) getParentFragment();
                            FragmentStatePagerAdapter fragmentStatePagerAdapter = myFragment.getAdapter();
                            fragmentStatePagerAdapter.notifyDataSetChanged();
                            mAdapter.notifyDataSetChanged();


        }

    });
                    builder.setNegativeButton("取消",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface arg0,int arg1) {

                        }
                    });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
