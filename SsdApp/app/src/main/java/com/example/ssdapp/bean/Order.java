package com.example.ssdapp.bean;

import com.example.ssdapp.bean.User;

/**
 * Created by 范晋瑜 on 2017/10/13.
 */

public class Order {
    private String id;
    private User user;
    private User ac_user;
    private String tag;//带外卖or带饭菜
    private String describe;
    private String state;//完成or未完成or未接单


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getAc_user() {
        return ac_user;
    }

    public void setAc_user(User ac_user) {
        this.ac_user = ac_user;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(int tag) {
        if(tag==1){
            this.tag = "带外卖";
        }
        else this.tag = "带饭菜";
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
