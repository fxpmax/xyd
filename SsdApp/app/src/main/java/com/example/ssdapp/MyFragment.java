package com.example.ssdapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by 范晋瑜 on 2017/10/9.
 */

public class MyFragment extends Fragment {

    private ViewPager viewPager;
    private List<Fragment> viewList;
    private FragmentStatePagerAdapter adapter;

    private TextView button_nodone, button_done, button_message;

    @Override
    public void onCreate(Bundle saveInstanceState) {

        super.onCreate(saveInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my, container, false);
        viewList = new ArrayList<>();
        viewList.add(new MyNodoneFragment());
        viewList.add(new MyDoneFragment());
        viewList.add(new MyMessageFragment());
        adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                return viewList.get(position);
            }

            @Override
            public int getCount() {
                return viewList.size();
            }

            @Override
            public int getItemPosition(Object object) {
                return PagerAdapter.POSITION_NONE;
            }
        };
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        button_nodone = (TextView) v.findViewById(R.id.button_nodone);
        button_nodone.setBackgroundColor(Color.parseColor("#FFFFFF"));
        button_nodone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
                button_nodone.setBackgroundColor(Color.parseColor("#FFFFFF"));
                button_done.setBackgroundColor(Color.parseColor("#FFFF00"));
                button_message.setBackgroundColor(Color.parseColor("#FFFF00"));
            }
        });
        button_done = (TextView) v.findViewById(R.id.button_done);
        button_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
                button_done.setBackgroundColor(Color.parseColor("#FFFFFF"));
                button_nodone.setBackgroundColor(Color.parseColor("#FFFF00"));
                button_message.setBackgroundColor(Color.parseColor("#FFFF00"));
            }
        });
        button_message = (TextView) v.findViewById(R.id.button_message);
        button_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPager.setCurrentItem(2);
                button_message.setBackgroundColor(Color.parseColor("#FFFFFF"));
                button_done.setBackgroundColor(Color.parseColor("#FFFF00"));
                button_nodone.setBackgroundColor(Color.parseColor("#FFFF00"));
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (positionOffsetPixels == 0 && positionOffset == 0) {

                }
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    button_nodone.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    button_done.setBackgroundColor(Color.parseColor("#FFFF00"));
                    button_message.setBackgroundColor(Color.parseColor("#FFFF00"));
                } else if (position == 1) {
                    button_done.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    button_nodone.setBackgroundColor(Color.parseColor("#FFFF00"));
                    button_message.setBackgroundColor(Color.parseColor("#FFFF00"));
                } else {
                    button_done.setBackgroundColor(Color.parseColor("#FFFF00"));
                    button_nodone.setBackgroundColor(Color.parseColor("#FFFF00"));
                    button_message.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;

    }


    public FragmentStatePagerAdapter getAdapter(){
        return  adapter;
    }
}
