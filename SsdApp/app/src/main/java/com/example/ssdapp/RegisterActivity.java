package com.example.ssdapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by 范晋瑜 on 2017/10/11.
 */

public class RegisterActivity extends AppCompatActivity{
    private Button mButton_register,mButton_cancel;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_register);

        mButton_register = (Button)findViewById(R.id.register_register);
        mButton_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(),"注册成功",Toast.LENGTH_SHORT).show();

                Intent i = new Intent(RegisterActivity.this,LogActivity.class);
                startActivity(i);
            }
        });
        mButton_cancel = (Button)findViewById(R.id.register_cancel);
        mButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this,LogActivity.class);
                startActivity(i);
            }
        });

    }
}
