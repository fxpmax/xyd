package com.example.ssdapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ssdapp.bean.Post;
import com.example.ssdapp.R;
import com.itheima.retrofitutils.ItheimaHttp;
import com.itheima.retrofitutils.Request;
import com.itheima.retrofitutils.listener.HttpResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 范晋瑜 on 2017/10/11.
 */

public class RegisterActivity extends AppCompatActivity{
    private String ok;
    private Button mButton_register,mButton_cancel;
    private EditText name,number,password,password_comfirm,floor,room,phone,introduce;
    private final String TAG =getClass().getSimpleName();
    private static  String register_url = "http://123.206.107.125:3333/signup";
    private String getName,getNumber,getPassword,getPassword_comfirm,getFloor,getRoom,getPhone,getIntroduce;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_register);

        InitView();
        mButton_register.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    getEditText();
                                                    register();
                                                }
                                            });
        mButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this,LogActivity.class);
                startActivity(i);
            }
        });

    }

    private void register() {
        if(getName.equals(""))show("姓名不能为空");
        else if(getName.equals(""))show("账号不能为空");
        else if(getPassword.equals(""))show("密码不能为空");
        else if(!getPassword.equals(getPassword_comfirm))show("两次密码输入不一致");
        else if(getFloor.equals(""))show("楼号不能为空");
        else if(getRoom.equals(""))show("寝室号不能为空");
        else
            toPost();
    }

    private void toPost() {
        Request request = ItheimaHttp.newPostRequest(register_url);

        request.putParams("usernick",getName);
        request.putParams("username",getNumber);
        request.putParams("userpassword",getPassword);
        request.putParams("userphone",getPhone);
        request.putParams("userbuildnumber",getFloor);
        request.putParams("userbedroomnumber",getRoom);
        request.putParams("userintroduce",getIntroduce);

        retrofit2.Call call = ItheimaHttp.send(request, new HttpResponseListener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("fjy", s);
                try {
                    JSONObject json = new JSONObject(s);
                    if(json.getString("ok").equals("1")){
                        show("注册成功");
                        Intent i = new Intent(RegisterActivity.this,LogActivity.class);
                        startActivity(i);
                    }
                    else show("注册失败");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    private void getEditText() {
        getName = name.getText().toString();
        getNumber = number.getText().toString();
        getPassword = password.getText().toString();
        getPassword_comfirm = password_comfirm.getText().toString();
        getFloor = floor.getText().toString();
        getRoom = room.getText().toString();
        getPhone = phone.getText().toString();
        getIntroduce = introduce.getText().toString();
    }

    private void InitView() {
        name=(EditText)findViewById(R.id.register_name);
        number=(EditText)findViewById(R.id.register_number);
        password=(EditText)findViewById(R.id.register_password);
        password_comfirm=(EditText)findViewById(R.id.register_password_confirm);
        floor=(EditText)findViewById(R.id.register_floor);
        room=(EditText)findViewById(R.id.register_room);
        phone=(EditText)findViewById(R.id.register_phone);
        introduce=(EditText)findViewById(R.id.register_introduce);
        password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        password_comfirm.setTransformationMethod(PasswordTransformationMethod.getInstance());

        mButton_register = (Button)findViewById(R.id.register_register);
        mButton_cancel = (Button)findViewById(R.id.register_cancel);
    }

    private void show(String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }
}
