package com.example.ssdapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ssdapp.Activity.AlterActivity;

/**
 * Created by 范晋瑜 on 2017/10/10.
 */

public class MyMessageFragment extends Fragment {
//    private String name,number,floor,room,phone,introduce;
    private Button button_alter;
    private TextView name,score,number,floor,room,phone,introduce;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_message, container, false);


        name=(TextView) v.findViewById(R.id.name);
        score=(TextView)v.findViewById(R.id.score) ;
        number=(TextView) v.findViewById(R.id.number);
        floor=(TextView) v.findViewById(R.id.floor);
        room=(TextView) v.findViewById(R.id.room);
        phone=(TextView) v.findViewById(R.id.phone);
        introduce=(TextView) v.findViewById(R.id.introduce);

       flush();

        button_alter =(Button)v.findViewById(R.id.alter);
        button_alter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AlterActivity.class);
                startActivityForResult(intent,1);
            }
        });
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        if(requestCode == 1&&resultCode == 2){
            flush();
        }
    }
    private void flush(){
//        name.setText(User.CurrentUser.get("name"));
//        score.setText();
//        number.setText(User.CurrentUser.get("number"));
//        floor.setText(User.CurrentUser.get("floor"));
//        room.setText(User.CurrentUser.get("room"));
//        phone.setText(User.CurrentUser.get("phone"));
//        introduce.setText(User.CurrentUser.get("introduce"));
    }



}

