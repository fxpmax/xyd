package com.example.ssdapp.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ssdapp.R;

/**
 * Created by 范晋瑜 on 2017/10/10.
 */

public class AlterActivity extends Activity {
    private Button mButton_commit,mButton_cancel;

    private EditText name,password,password_confirm,floor,room,phone,introduce;
    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.fragment_alter);

        name=(EditText)findViewById(R.id.alter_name);
        password=(EditText)findViewById(R.id.alter_password);
        password_confirm=(EditText)findViewById(R.id.alter_password_confirm);
        floor=(EditText)findViewById(R.id.alter_floor);
        room=(EditText)findViewById(R.id.alter_room);
        phone=(EditText)findViewById(R.id.alter_phone);
        introduce=(EditText)findViewById(R.id.alter_introduce);

        password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        password_confirm.setTransformationMethod(PasswordTransformationMethod.getInstance());

        mButton_commit = (Button)findViewById(R.id.commit);
        mButton_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getName = name.getText().toString();
                String getPassword = password.getText().toString();
                String getPassword_comfirm = password_confirm.getText().toString();
                String getFloor = floor.getText().toString();
                String getRoom = room.getText().toString();
                String getPhone = phone.getText().toString();
                String getIntroduce = introduce.getText().toString();

                if(getName.equals(""))show("姓名不能为空");
                else if(getPassword.equals(""))show("密码不能为空");
                else if(!getPassword.equals(getPassword_comfirm))show("两次密码输入不一致");
                else if(getFloor.equals(""))show("楼号不能为空");
                else if(getRoom.equals(""))show("寝室号不能为空");
                else {;
                    show("修改成功");
                    setResult(2);
                    finish();
                }

            }
        });
        mButton_cancel = (Button)findViewById(R.id.cancel);
        mButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(3);
                finish();
            }
        });

   }
        private void show(String s){
            Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
        }
}
