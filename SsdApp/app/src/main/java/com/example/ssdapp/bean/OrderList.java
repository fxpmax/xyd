package com.example.ssdapp.bean;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 范晋瑜 on 2017/11/2.
 */

public class OrderList {
    private static OrderList sOrderList;
    private List<Order> mOrders;

    public OrderList(Context context) {
        mOrders = new ArrayList<>();
        for(int i=0;i<100;i++){
            Order order = new Order();
            User user = new User();
            user.setName("user #"+i);
            user.setFloor("floor &"+i*2);
            user.setRoom("room $"+(i+100));

            order.setUser(user);
            order.setTag(i%2);
            order.setId("id:"+i);
            mOrders.add(order);
        }
    }

    public static OrderList get(Context context){
        if(sOrderList == null){
            sOrderList = new OrderList(context);
        }
        return sOrderList;
    }

    public List<Order> getOrders() {
        return mOrders;
    }
}
